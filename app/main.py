import asyncio
import time
from datetime import datetime
from typing import List

import aiohttp
import matplotlib.pyplot as plt
import numpy
from fastapi import FastAPI, Header, HTTPException
from fastapi.responses import FileResponse
from pydantic import BaseModel
from scipy.optimize import minimize

app = FastAPI()


class MacInfo(BaseModel):
    hash: str = None
    strength: int = None


class RpiPayload(BaseModel):
    data: List[MacInfo] = []
    time: datetime = None


async def update_backend_heatmap(coordinaten):
    url = "http://api.happypoint.works/devices/Heidelberglaan%2015"
    api_key = "ded6c97a44536076a017c852936a3a6c0086034325b939887b2ca1a97cc90da9"
    headers = {"Authorization": f"Bearer {api_key}"}
    payload = [coordinaten]  # todo link room name to api key, add datetime
    print(payload)
    async with aiohttp.ClientSession() as session:
        async with session.post(url, headers=headers, json=payload) as r:
            print(await r.text(), r.status)


class RpiData:
    def __init__(self):
        self.devicedata = {
            "cd4b3a06aa4520f145ed96b06577ddda": {"coords": (6, 7), "macs": {}},
            "e8d07503fc1c03aa72c73ba14354f4b6": {"coords": (6, 1), "macs": {}},
            "a3e3ab9d3219f8c37e9af9c3517672f4": {"coords": (1, 5), "macs": {}},
            "b6d63a6362618cfbb9baebf2f5abfa11": {"coords": (0, 4), "macs": {}}
        }

    async def timeouts(self):
        """Remove old, obsolete datapoints that would not yield a valuable trilateration"""
        while True:
            for apikey, data in dict(self.devicedata).items():
                macs = data["macs"]
                for mac, macdata in dict(macs).items():
                    if (time.time() - macdata['epoch']) > 120:  # remove data older than 2 minutes
                        del self.devicedata[apikey]['macs'][mac]
            await asyncio.sleep(1)

    async def dispatch(self):
        """"Iterate over received data and calculate the device location once enough data points are found"""
        while True:
            found = {}
            for apikey, data in dict(self.devicedata).items():
                macs = data["macs"]
                for mac, macdata in macs.items():
                    if mac not in found:
                        found[mac] = {}
                    found[mac][apikey] = {"coords": data["coords"], "rssi": macdata["rssi"]}
            for mac, data in found.items():
                print(len(data))
                if len(data) >= 3:  # min amount of devices
                    print("Dispatching")
                    await Trilaterate(data).start()
                    for apikey in data.keys():
                        del self.devicedata[apikey]["macs"][mac]
            await asyncio.sleep(15)


class Trilaterate:
    """Input: Minimum 3 drone coordinates
    Output: x, y of device location"""

    def __init__(self, data):
        self.rpi_locations = [[x["coords"][0], x["coords"][1], x["rssi"]] for x in data.values()]

    @staticmethod
    def get_distance(RSSI, transmit_power=18, path_loss_exponent=7):  # Todo test
        return 10 ** ((transmit_power - RSSI) / (10 * path_loss_exponent))

    @staticmethod
    def mse(x, location_distance):
        # The mean square error function which is used to find the point x later in the optimization algorithm
        mean_square_error = 0.0
        for point in location_distance:
            calc_distance = numpy.sqrt(numpy.power(x[0] - point[0], 2) + numpy.power(x[1] - point[1], 2))
            mean_square_error += numpy.power(calc_distance - point[2], 2.0)
        return mean_square_error / len(location_distance)

    def guess(self):
        # sets the initual guess for the optimization algorithm. the initial guess is the closest point to the target
        min_distance = float("inf")
        closest_location = None
        for data in self.rpi_locations:  # A new closest point!
            if data[2] < min_distance:
                min_distance = data[2]
                closest_location = [data[0], data[1]]
        guessed_location = numpy.array(closest_location)
        return guessed_location

    def calculate_location(self, guessed_location):
        location = minimize(
            self.mse,  # The error function
            guessed_location,  # The initial guess
            args=self.rpi_locations,  # Additional parameters for mse
            method="L-BFGS-B",  # The optimisation algorithm
            options={
                # 'tol':1e-7,         # Tolerance
                "maxiter": 1e20  # Maximum iterations
            },
        )
        return location

    def plot(self, calculated_location):
        circle2 = plt.Circle(
            (calculated_location.x[1], calculated_location.x[0]),
            0.4,
            color="r",
            fill=True,
        )
        c3 = plt.Circle(
            (self.rpi_locations[0][1], self.rpi_locations[0][0]),
            self.rpi_locations[0][2],
            color="b",
            fill=False,
        )
        c4 = plt.Circle(
            (self.rpi_locations[1][1], self.rpi_locations[1][0]),
            self.rpi_locations[1][2],
            color="b",
            fill=False,
        )
        c5 = plt.Circle(
            (self.rpi_locations[2][1], self.rpi_locations[2][0]),
            self.rpi_locations[2][2],
            color="b",
            fill=False,
        )
        fig, ax = plt.subplots()
        ax.set_xlim((-10, 25))
        ax.set_ylim((-10, 25))
        ax.add_artist(circle2)
        # ax.add_artist(circle1)
        ax.add_artist(c3)
        ax.add_artist(c4)
        ax.add_artist(c5)
        ax.set_aspect(1)
        #fig.savefig(f"{time.time()}.png")
        fig.savefig('plotcircles2.png')

    async def start(self):
        print(f"calculating: {self.rpi_locations}")
        guessed_location = self.guess()
        location = self.calculate_location(guessed_location)
        self.plot(location)
        #await update_backend_heatmap(f"{int(location.x[1])},{int(location.x[0])},0")


@app.post("/macs")
async def update_macs(rpipayload: RpiPayload, Authorization: str = Header(None)):
    api_key = Authorization.split("Bearer ")[1]
    if api_key not in rpidata.devicedata:
        raise HTTPException(status_code=401, detail="Invalid API key")
    payload = rpipayload.dict()
    for x in payload['data']:
        rpidata.devicedata[api_key]['macs'][x['hash']] = {"rssi": x['strength'], "epoch": time.time()}
    # pprint(rpidata.devicedata)
    return rpidata.devicedata


@app.get("/plot")
async def show_plot():
    return FileResponse("plotcircles2.png", media_type="image/png")

rpidata = RpiData()
asyncio.create_task(rpidata.timeouts())
asyncio.create_task(rpidata.dispatch())
"""
uvicorn main:app --reload
"""
