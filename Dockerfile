FROM python:3.8

RUN pip install fastapi uvicorn aiohttp aiofiles scipy numpy matplotlib

EXPOSE 80

COPY ./app /app

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80","--proxy-headers"]

