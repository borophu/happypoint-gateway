import secrets
import time
from datetime import datetime
from random import randint

import requests

mac1 = secrets.token_hex(32)
mac2 = secrets.token_hex(32)
mac3 = secrets.token_hex(32)
mac4 = secrets.token_hex(32)


def main(api_key):
    # Identify with API key or seperate identifier?
    url = "https://happypoint.works/macs"
    headers = {"Authorization": f"Bearer {api_key}"}
    payload = {
        "data": [
            {"hash": mac1, "strength": randint(1, 5)},
            {"hash": mac2, "strength": randint(1, 5)},
            {"hash": mac3, "strength": randint(1, 5)},
            {"hash": mac4, "strength": randint(1, 5)},
        ],
        "time": datetime.now().isoformat(),  # datetime individual per device
    }
    r = requests.post(url, headers=headers, json=payload)
    print(r.status_code)
    print(r.text)
    #print(r.json())


if __name__ == "__main__":
    #while True:
    main("cd4b3a06aa4520f145ed96b06577ddda")
    main("e8d07503fc1c03aa72c73ba14354f4b6")
    main("a3e3ab9d3219f8c37e9af9c3517672f4")
    main("cd4b3a06aa4520f145ed96b06577ddda")
        #time.sleep(1)
